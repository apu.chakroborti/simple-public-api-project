package com.apu.sms.ws.user.bean;

import com.apu.sms.business.user.bean.UserManager;
import com.apu.sms.business.user.entity.User;
import com.apu.sms.ws.student.entity.ServiceError;
import com.apu.sms.ws.user.request.CreateUserServiceRequest;
import com.apu.sms.ws.user.request.ReadUserServiceRequest;
import com.apu.sms.ws.user.response.CreateUserServiceResponse;
import com.apu.sms.ws.user.response.ReadUserServiceResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import java.util.List;

@Stateless(name="UserService",mappedName = "SMSService-SMSService-UserService")
@WebService(name="UserService",serviceName = "UserService",portName = "UserServicePort")
@HandlerChain(file="../../common/handler/UserServiceSecurityHandler.xml")
public class UserServiceBean implements UserService{

    @Resource
    private WebServiceContext ctx;

    @EJB
    private UserManager userManager;

    private static Logger LOGGER = null;

    public UserServiceBean(){
        if(LOGGER==null){
            LOGGER= LogManager.getLogger("UserServiceBean");
        }
    }

    @Override
    @WebMethod
    public CreateUserServiceResponse createUser(@WebParam(name = "request") CreateUserServiceRequest request) {

        if(request==null){
            LOGGER.info("resquest is null");
            return new CreateUserServiceResponse(new ServiceError(404,"request is null"));
        }
        //System.out.println("StudentService:ws:Student created");
        LOGGER.info("UserService:ws:User created");
        Object object=userManager.createUser(request.getUser());

        if(object instanceof ServiceError){
            LOGGER.info("Service Error");
            return new CreateUserServiceResponse((ServiceError) object);
        }
        else if(object instanceof User){
            LOGGER.info("Create Student success");
            return new CreateUserServiceResponse((User) object);
        }
        return null;
    }

    @Override
    @WebMethod
    public ReadUserServiceResponse searchUser(@WebParam(name = "request") ReadUserServiceRequest request) {

        //System.out.println("StudentService:ws:Student read");
        LOGGER.info("StudentService:ws:Student read");
        Object object=userManager.searchUser(request.getSearchUserCriteria());

        if(object instanceof ServiceError){
            return new ReadUserServiceResponse((ServiceError)object);
        }
        List<User> list=(List<User>)object;
        //Utils.println("In sevice layer read object size:"+list.size());
        LOGGER.info("In sevice layer read object size:"+list.size());
        return new ReadUserServiceResponse((List<User>)object);

    }

}
