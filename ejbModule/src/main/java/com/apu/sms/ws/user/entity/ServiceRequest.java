package com.apu.sms.ws.user.entity;

import java.io.Serializable;

public class ServiceRequest implements Serializable {
    private String requestHeader;

    public ServiceRequest(String requestHeader) {
        this.requestHeader = requestHeader;
    }

    public ServiceRequest() {
    }

    public String getRequestHeader() {
        return requestHeader;
    }

    public void setRequestHeader(String requestHeader) {
        this.requestHeader = requestHeader;
    }
}
