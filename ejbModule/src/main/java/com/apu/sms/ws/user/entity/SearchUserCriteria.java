package com.apu.sms.ws.user.entity;

import java.io.Serializable;


public class SearchUserCriteria implements Serializable {
    private Integer id;
    private boolean totalCountRequired;
    private Integer startIndex;
    private Integer maxResult;

    public SearchUserCriteria(Integer id, boolean totalCountRequired, Integer startIndex, Integer maxResult) {
        this.id = id;
        this.totalCountRequired = totalCountRequired;
        this.startIndex = startIndex;
        this.maxResult = maxResult;
    }

    public SearchUserCriteria() {
     }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isTotalCountRequired() {
        return totalCountRequired;
    }

    public void setTotalCountRequired(boolean totalCountRequired) {
        this.totalCountRequired = totalCountRequired;
    }

    public Integer getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(Integer startIndex) {
        this.startIndex = startIndex;
    }

    public Integer getMaxResult() {
        return maxResult;
    }

    public void setMaxResult(Integer maxResult) {
        this.maxResult = maxResult;
    }
}
