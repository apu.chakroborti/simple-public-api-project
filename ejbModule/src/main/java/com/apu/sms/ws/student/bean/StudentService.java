package com.apu.sms.ws.student.bean;

import com.apu.sms.ws.student.response.ReadStudentServiceResponse;
import com.apu.sms.ws.student.request.CreateStudentServiceRequest;
import com.apu.sms.ws.student.request.ReadStudentServiceRequest;
import com.apu.sms.ws.student.response.CreateStudentServiceResponse;

import javax.ejb.Remote;

@Remote
public interface StudentService {
    CreateStudentServiceResponse createStudent(CreateStudentServiceRequest request);
    ReadStudentServiceResponse readStudent(ReadStudentServiceRequest request);
    String updateStudent(String id);
    String deleteStudent(String id);

}
