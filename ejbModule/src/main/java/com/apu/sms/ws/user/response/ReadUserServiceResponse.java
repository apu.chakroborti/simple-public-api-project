package com.apu.sms.ws.user.response;

import com.apu.sms.business.student.entity.Student;
import com.apu.sms.business.user.entity.User;
import com.apu.sms.ws.student.entity.ServiceError;
import com.apu.sms.ws.student.entity.ServiceResponse;

import java.io.Serializable;
import java.util.List;

public class ReadUserServiceResponse extends ServiceResponse implements Serializable {

    private List<User> userList;

    public ReadUserServiceResponse(ServiceError serviceError, List<User> userList) {
        super(serviceError);
        this.userList=userList;
    }
    public ReadUserServiceResponse(ServiceError serviceError) {
        super(serviceError);

    }

    public ReadUserServiceResponse() {
        super();

    }
    public ReadUserServiceResponse(List<User> userList) {
        this.userList = userList;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
