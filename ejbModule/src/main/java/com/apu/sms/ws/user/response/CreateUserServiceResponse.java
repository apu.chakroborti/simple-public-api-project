package com.apu.sms.ws.user.response;

import com.apu.sms.business.student.entity.Student;
import com.apu.sms.business.user.entity.User;
import com.apu.sms.ws.student.entity.ServiceError;
import com.apu.sms.ws.student.entity.ServiceResponse;

import java.io.Serializable;

public class CreateUserServiceResponse extends ServiceResponse implements Serializable {

    private User user;


    public CreateUserServiceResponse(ServiceError serviceError) {
        super(serviceError);

    }
    public CreateUserServiceResponse(User user) {
        super();
        this.user=user;
    }
    public CreateUserServiceResponse() {
        super();

    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
