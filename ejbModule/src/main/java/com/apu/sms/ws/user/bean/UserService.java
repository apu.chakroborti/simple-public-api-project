package com.apu.sms.ws.user.bean;

import com.apu.sms.ws.user.request.CreateUserServiceRequest;
import com.apu.sms.ws.user.request.ReadUserServiceRequest;
import com.apu.sms.ws.user.response.CreateUserServiceResponse;
import com.apu.sms.ws.user.response.ReadUserServiceResponse;

import javax.ejb.Remote;

@Remote
public interface UserService {
    CreateUserServiceResponse createUser(CreateUserServiceRequest request);
    ReadUserServiceResponse searchUser(ReadUserServiceRequest request);
    /*String updateUser(String id);
    String deleteUser(String id);*/
}
