package com.apu.sms.ws.student.request;



import com.apu.sms.business.student.entity.Student;
import com.apu.sms.ws.student.entity.ServiceRequest;

import java.io.Serializable;

public class CreateStudentServiceRequest extends ServiceRequest implements Serializable{
    private Student student;

    public CreateStudentServiceRequest(String requestHeader, Student student) {
        super(requestHeader);
        this.student = student;
    }

    public CreateStudentServiceRequest() {
        super();

    }
    public CreateStudentServiceRequest(Student student) {
        this.student = student;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
