package com.apu.sms.ws.common.handler;

import com.apu.sms.business.common.def.Defs;
import com.apu.sms.business.common.util.Utils;
import org.apache.logging.log4j.ThreadContext;

import javax.xml.namespace.QName;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.util.Set;

/**
 * Created by shibli on 18/12/18.
 */
public class StudentServiceSecurityHandler implements SOAPHandler<SOAPMessageContext> {
    @Override
    public Set<QName> getHeaders() {
        Utils.println("getHeaders executed");
        return null;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        Utils.println("handle message executed");
        context.put(Defs.CONTEXT_USER_SESSION_UUID,Utils.getNewUuid());
        ThreadContext.put(Defs.CONTEXT_USER_SESSION_UUID,Utils.getNewUuid());
        //MDC.put(Defs.CONTEXT_USER_SESSION_UUID,Utils.getNewUuid());
        //MDC.put(Defs.CONTEXT_USER_SESSION_UUID,"LoggerTestId");
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        Utils.println("handle fault executed");
        return false;
    }

    @Override
    public void close(MessageContext context) {
        Utils.println("close executed");
    }
}
