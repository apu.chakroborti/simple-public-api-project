package com.apu.sms.ws.redis.bean;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

@Stateless(name="RedisService",mappedName = "SMSService-SMSService-RedisService")
@WebService(name="RedisService",serviceName = "RedisService",portName = "RedisServicePort")
@HandlerChain(file="../../common/handler/UserServiceSecurityHandler.xml")
public class RedisServiceBean implements RedisService {

    @Resource
    private WebServiceContext ctx;

    private static Logger LOGGER = null;

    public RedisServiceBean(){
        if(LOGGER==null){
            LOGGER= LogManager.getLogger("RedisServiceBean");
        }
    }


    @Override
    @WebMethod
    public String showTestRedisData(@WebParam(name = "request") String val) {
        return "Not implemented yet";
    }
}
