package com.apu.sms.ws.user.request;

import com.apu.sms.ws.student.entity.SearchStudentCriteria;
import com.apu.sms.ws.student.entity.ServiceRequest;
import com.apu.sms.ws.user.entity.SearchUserCriteria;

import java.io.Serializable;

public class ReadUserServiceRequest extends ServiceRequest implements Serializable{
    private SearchUserCriteria searchUserCriteria;

    public ReadUserServiceRequest(String requestHeader, SearchUserCriteria searchUserCriteria) {
        super(requestHeader);
        this.searchUserCriteria = searchUserCriteria;
    }

    public ReadUserServiceRequest(SearchUserCriteria searchUserCriteria) {
        this.searchUserCriteria=searchUserCriteria;
    }

    public ReadUserServiceRequest() {
        super();
    }


    public SearchUserCriteria getSearchUserCriteria() {
        return searchUserCriteria;
    }

    public void setSearchUserCriteria(SearchUserCriteria searchUserCriteria) {
        this.searchUserCriteria = searchUserCriteria;
    }
}
