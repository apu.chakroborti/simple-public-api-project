package com.apu.sms.ws.user.request;




import com.apu.sms.business.user.entity.User;
import com.apu.sms.ws.student.entity.ServiceRequest;

import java.io.Serializable;

public class CreateUserServiceRequest extends ServiceRequest implements Serializable{
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
