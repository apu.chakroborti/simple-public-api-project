package com.apu.sms.ws.student.request;

import com.apu.sms.ws.student.entity.SearchStudentCriteria;
import com.apu.sms.ws.student.entity.ServiceRequest;

import java.io.Serializable;

public class ReadStudentServiceRequest extends ServiceRequest implements Serializable{
    private SearchStudentCriteria searchStudentCriteria;

    public ReadStudentServiceRequest(String requestHeader, SearchStudentCriteria searchStudentCriteria) {
        super(requestHeader);
        this.searchStudentCriteria = searchStudentCriteria;
    }

    public ReadStudentServiceRequest(SearchStudentCriteria searchStudentCriteria) {
        this.searchStudentCriteria = searchStudentCriteria;
    }

    public ReadStudentServiceRequest() {
        super();
    }

    public SearchStudentCriteria getSearchStudentCriteria() {
        return searchStudentCriteria;
    }

    public void setSearchStudentCriteria(SearchStudentCriteria searchStudentCriteria) {
        this.searchStudentCriteria = searchStudentCriteria;
    }
}
