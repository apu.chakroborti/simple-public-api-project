package com.apu.sms.ws.user.entity;

import java.io.Serializable;

public class ServiceResponse implements Serializable{
    private ServiceError serviceError;

    public ServiceResponse(ServiceError serviceError) {
        this.serviceError = serviceError;
    }
    public ServiceResponse() {

    }

    public ServiceError getServiceError() {
        return serviceError;
    }

    public void setServiceError(ServiceError serviceError) {
        this.serviceError = serviceError;
    }
}
