package com.apu.sms.ws.redis.bean;

import javax.ejb.Remote;

@Remote
public interface RedisService {

    String showTestRedisData(String val);
}
