package com.apu.sms.ws.student.entity;


import java.io.Serializable;

public class ServiceError implements Serializable {
    private Integer errorCode;
    private String message;

    public ServiceError(Integer errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }
    public ServiceError() {

    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
