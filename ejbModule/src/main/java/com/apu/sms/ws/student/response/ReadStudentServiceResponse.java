package com.apu.sms.ws.student.response;

import com.apu.sms.business.student.entity.Student;
import com.apu.sms.ws.student.entity.ServiceError;
import com.apu.sms.ws.student.entity.ServiceResponse;

import java.io.Serializable;
import java.util.List;

public class ReadStudentServiceResponse extends ServiceResponse implements Serializable {

    private List<Student> student;

    public ReadStudentServiceResponse(ServiceError serviceError, List<Student> student) {
        super(serviceError);
        this.student = student;
    }
    public ReadStudentServiceResponse(ServiceError serviceError) {
        super(serviceError);

    }

    public ReadStudentServiceResponse() {
        super();

    }
    public ReadStudentServiceResponse(List<Student> student) {
        this.student = student;
    }

    public List<Student> getStudent() {
        return student;
    }

    public void setStudent(List<Student> student) {
        this.student = student;
    }
}
