package com.apu.sms.ws.student.response;

import com.apu.sms.business.student.entity.Student;
import com.apu.sms.ws.student.entity.ServiceError;
import com.apu.sms.ws.student.entity.ServiceResponse;

import java.io.Serializable;

public class CreateStudentServiceResponse extends ServiceResponse implements Serializable {

    private Student student;


    public CreateStudentServiceResponse(ServiceError serviceError) {
        super(serviceError);

    }
    public CreateStudentServiceResponse(Student student) {
        super();
        this.student = student;
    }
    public CreateStudentServiceResponse() {
        super();

    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
