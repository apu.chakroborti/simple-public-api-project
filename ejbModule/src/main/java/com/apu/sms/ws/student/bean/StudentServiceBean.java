package com.apu.sms.ws.student.bean;


import com.apu.sms.business.student.bean.StudentManager;
import com.apu.sms.business.student.entity.Student;
import com.apu.sms.ws.student.entity.ServiceError;
import com.apu.sms.ws.student.request.CreateStudentServiceRequest;
import com.apu.sms.ws.student.request.ReadStudentServiceRequest;
import com.apu.sms.ws.student.response.CreateStudentServiceResponse;
import com.apu.sms.ws.student.response.ReadStudentServiceResponse;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Stateless(name="StudentService",mappedName = "SMSService-SMSService-StudentService")
@WebService(name="StudentService",serviceName = "StudentService",portName = "StudentServicePort")
@HandlerChain(file="../../common/handler/StudentServiceSecurityHandler.xml")
public class StudentServiceBean implements StudentService {

    @Resource
    private WebServiceContext ctx;

    @EJB
    private StudentManager studentManager;

    private static Logger LOGGER = null;

    public StudentServiceBean(){
        if(LOGGER==null){
            LOGGER=LogManager.getLogger("StudentServiceBean");
        }
    }

    @Override
    @WebMethod
    public CreateStudentServiceResponse createStudent(@WebParam(name = "request")CreateStudentServiceRequest request) {

        if(request==null){
            LOGGER.info("resquest is null");
            return new CreateStudentServiceResponse(new ServiceError(404,"request is null"));
        }
        //System.out.println("StudentService:ws:Student created");
        LOGGER.info("StudentService:ws:Student created");
        Object object=studentManager.createStudent(request.getStudent());

        if(object instanceof ServiceError){
            LOGGER.info("Service Error");
            return new CreateStudentServiceResponse((ServiceError) object);
        }
        else if(object instanceof Student){
            LOGGER.info("Create Student success");
            return new CreateStudentServiceResponse((Student) object);
        }
        return null;
    }

    @Override
    @WebMethod
    public ReadStudentServiceResponse readStudent(@WebParam(name = "request")ReadStudentServiceRequest request) {
        //System.out.println("StudentService:ws:Student read");
        LOGGER.info("StudentService:ws:Student read");
        Object object=studentManager.readStudent(request.getSearchStudentCriteria());

        if(object instanceof ServiceError){
            return new ReadStudentServiceResponse((ServiceError)object);
        }
        List<Student> list=(List<Student>)object;
        //Utils.println("In sevice layer read object size:"+list.size());
        LOGGER.info("In sevice layer read object size:"+list.size());
        return new ReadStudentServiceResponse((List<Student>)object);

    }

    @Override
    @WebMethod
    public String updateStudent(@WebParam(name = "request")String id) {
        //System.out.println("StudentService:ws:Student updated");
        LOGGER.info("StudentService:ws:Student updated");
        return "Student updated"+" id:"+id;
    }

    @Override
    @WebMethod
    public String deleteStudent(@WebParam(name = "request")String id) {
        //System.out.println("StudentService:ws:Student deleted");
        LOGGER.info("StudentService:ws:Student deleted");
        return "Student deleted"+" id:"+id;
    }
}
