package com.apu.sms.business.student.bean;


import com.apu.sms.business.common.util.ErrorCode;
import com.apu.sms.business.common.util.Utils;
import com.apu.sms.database.bean.BaseEntityManager;
import com.apu.sms.database.entity.student.Student;
import com.apu.sms.ws.student.entity.SearchStudentCriteria;
import com.apu.sms.ws.student.entity.ServiceError;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Stateless(name = "StudentBM", mappedName = "SMSService-SMSService-StudentBM")
public class StudentManagerBean implements StudentManager {
    private static Logger LOGGER;

    public StudentManagerBean() {

    }

    @EJB
    private BaseEntityManager baseEntityManager;

    @Override
    public Object createStudent(com.apu.sms.business.student.entity.Student student) {

        if (student == null) {
            return new ServiceError(ErrorCode.NULL_VALUE, "student information is null");
        }
        Student studentEO = new Student();

        Integer id = baseEntityManager.getSequence("student_id_seq");
        //Integer id=10;
        if (!Utils.isNull(id)) {
            studentEO.setId(id);
            student.setId(id);
        }

        if (!Utils.isEmpty(student.getFirstName())) {
            studentEO.setFirstName(student.getFirstName());
        }

        if (!Utils.isEmpty(student.getLastName())) {
            studentEO.setLastName(student.getLastName());
        }

        if (!Utils.isNull(student.getRollNo())) {
            studentEO.setRollNo(student.getRollNo());
        }

        if (!Utils.isNull(student.getLevelNo())) {
            studentEO.setLevel(student.getLevelNo());
        }

        try {
            Object object = baseEntityManager.persistStudent(studentEO);
        } catch (Exception e) {
            LOGGER.info("create student exception occur" + e);
        }

        return student;

    }

    @Override
    public Object readStudent(SearchStudentCriteria searchStudentCriteria) {

        List<Object[]> resultSet;
        try {
            resultSet = baseEntityManager.findByQuery(searchStudentCriteria);
        } catch (Exception e) {
            return new ServiceError(404, "Exception Occur while Reading!!!!");
        }


        List<com.apu.sms.business.student.entity.Student> studentList = new ArrayList<com.apu.sms.business.student.entity.Student>();

        try {
            if (resultSet != null && resultSet.size() > 0) {
                Utils.println("size of result set:" + resultSet.size());
                for (int i = 0; i < resultSet.size(); i++) {
                    Object[] objects = resultSet.get(i);
                    com.apu.sms.business.student.entity.Student student = new com.apu.sms.business.student.entity.Student();
                    student.setId(Utils.ObjectToInteger(objects[0]));
                    student.setFirstName((String) objects[1]);
                    student.setLastName((String) objects[2]);
                    student.setRollNo(Utils.ObjectToInteger(objects[3]));
                    student.setLevelNo(Utils.ObjectToInteger(objects[4]));
                    studentList.add(student);
                    Utils.println(Utils.ObjectToInteger(objects[0]) + "," + (String) objects[1] + "," +
                            (String) objects[2] + "," + Utils.ObjectToInteger(objects[3]) + Utils.ObjectToInteger(objects[4]));
                }
                Utils.println("size of studentList:" + studentList.size());
                return studentList;
            } else {
                return new ServiceError(404, "No Student Found");
            }
        } catch (Exception e) {
            return new ServiceError(404, "Exception Occur while List Creating!!!");
        }


    }

    @Override
    public Object readStudentOrderByName(SearchStudentCriteria searchStudentCriteria) {


        List<Student> studentEo = null;
        try {
            studentEo = baseEntityManager.findByCriteriaQuery(searchStudentCriteria);
        } catch (Exception e) {
            LOGGER.info("Exception occcur" + e);
            return new ServiceError(404,"Exception Occur while List Creating!!!");
        }

        List<com.apu.sms.business.student.entity.Student> studentBo = null;
        if (studentEo != null && studentEo.size() > 0) {
            com.apu.sms.business.student.entity.Student student = new com.apu.sms.business.student.entity.Student();
            for (Student studentE : studentEo) {
                student.setFirstName(studentE.getFirstName());
                student.setLastName(studentE.getLastName());
                student.setLevelNo(studentE.getLevel());
                student.setRollNo(studentE.getRollNo());
                student.setId(studentE.getId());
                studentBo.add(student);
            }
        }
        return studentBo;

        /*List<Object[]> resultSet;
        try{
            resultSet=baseEntityManager.findByCriteriaQuery(searchStudentCriteria);
        }
        catch(Exception e){
            return new ServiceError(404,"Exception Occur while Reading!!!!");
        }


        List<Student> studentList=new ArrayList<Student>();

        try{
            if(resultSet!=null && resultSet.size()>0){
                Utils.println("size of result set:"+resultSet.size());
                for(int i=0;i<resultSet.size();i++){
                    Object[] objects=resultSet.get(i);
                    Student student=new Student();
                    student.setId(Utils.ObjectToInteger(objects[0]));
                    student.setFirstName((String)objects[1]);
                    student.setLastName((String)objects[2]);
                    student.setRollNo(Utils.ObjectToInteger(objects[3]));
                    student.setLevelNo(Utils.ObjectToInteger(objects[4]));
                    studentList.add(student);
                    Utils.println(Utils.ObjectToInteger(objects[0])+","+(String)objects[1]+","+
                            (String)objects[2]+","+Utils.ObjectToInteger(objects[3])+Utils.ObjectToInteger(objects[4]));
                }
                Utils.println("size of studentList:"+studentList.size());
                return studentList;
            }
            else{
                return new ServiceError(404,"No Student Found");
            }
        }catch(Exception e){
            return new ServiceError(404,"Exception Occur while List Creating!!!");
        }*/


    }

    @Override
    public void updateStudent(com.apu.sms.business.student.entity.Student student) {

    }

    @Override
    public void deleteStudent(Integer id) {

    }

}
