package com.apu.sms.business.user.bean;

import com.apu.sms.business.user.entity.User;
import com.apu.sms.ws.user.entity.SearchUserCriteria;

import javax.ejb.Remote;

@Remote
public interface UserManager {

    Object createUser(User user);
    Object searchUser(SearchUserCriteria searchUserCriteria);
    /*Object readStudentOrderByName(SearchStudentCriteria searchStudentCriteria);
    void updateStudent(Student student);
    void deleteStudent(Integer id);*/
}
