package com.apu.sms.business.user.bean;


import com.apu.sms.business.common.util.ErrorCode;
import com.apu.sms.business.common.util.Utils;
import com.apu.sms.database.bean.BaseEntityManager;
import com.apu.sms.database.entity.student.Student;
import com.apu.sms.database.entity.user.User;
import com.apu.sms.ws.student.entity.SearchStudentCriteria;
import com.apu.sms.ws.student.entity.ServiceError;
import com.apu.sms.ws.user.entity.SearchUserCriteria;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Stateless(name = "UserBM", mappedName = "SMSService-SMSService-UserBM")
public class UserManagerBean implements UserManager {
    private static Logger LOGGER;

    public UserManagerBean() {

    }

    @EJB
    private BaseEntityManager baseEntityManager;

    @Override
    public Object createUser(com.apu.sms.business.user.entity.User userBO) {

        if (userBO == null) {
            return new ServiceError(ErrorCode.NULL_VALUE, "user information is null");
        }
        User userEO = new User();

        if(userBO.getUserName()!=null){
            userEO.setUserName(userBO.getUserName());
        }
        if(userBO.getFirstName()!=null){
            userEO.setFirstName(userBO.getFirstName());
        }
        if(userBO.getLastName()!=null){
            userEO.setLastName(userBO.getLastName());
        }
        if(userBO.getAge()!=null){
            userEO.setAge(userBO.getAge());
        }
        if(userBO.getPassword()!=null){
            userEO.setPassword(userBO.getPassword());
        }

        try {
            Object object = baseEntityManager.persistUser(userEO);
        } catch (Exception e) {
            LOGGER.info("create user exception occur" + e);
        }

        return userBO;

    }

    @Override
    public Object searchUser(SearchUserCriteria searchUserCriteria) {

        List<Object[]> resultSet;
        try {
            resultSet = baseEntityManager.findByUserQuery(searchUserCriteria);
        } catch (Exception e) {
            return new ServiceError(404, "Exception Occur while Reading!!!!");
        }


        List<com.apu.sms.business.user.entity.User> userList = new ArrayList<com.apu.sms.business.user.entity.User>();

        try {
            if (resultSet != null && resultSet.size() > 0) {
                Utils.println("size of result set:" + resultSet.size());
                for (int i = 0; i < resultSet.size(); i++) {
                    Object[] objects = resultSet.get(i);
                    com.apu.sms.business.user.entity.User user= new com.apu.sms.business.user.entity.User();
                    user.setId(Utils.ObjectToInteger(objects[0]));
                    user.setUserName((String)objects[1]);
                    user.setFirstName((String) objects[2]);
                    user.setLastName((String) objects[3]);
                    user.setAge(Utils.ObjectToInteger(objects[4]));
                    user.setPassword((String)objects[5]);
                    userList.add(user);
                    Utils.println(Utils.ObjectToInteger(objects[0]) + "," + (String) objects[1] + "," +
                            (String) objects[2] + "," +(String) objects[3] + "," + Utils.ObjectToInteger(objects[4]) + (String) objects[5]);
                }
                Utils.println("size of usertList:" + userList.size());
                return userList;
            } else {
                return new ServiceError(404, "No Student Found");
            }
        } catch (Exception e) {
            return new ServiceError(404, "Exception Occur while List Creating!!!");
        }


    }

   /* @Override
    public Object readStudentOrderByName(SearchStudentCriteria searchStudentCriteria) {


        List<Student> studentEo = null;
        try {
            studentEo = baseEntityManager.findByCriteriaQuery(searchStudentCriteria);
        } catch (Exception e) {
            LOGGER.info("Exception occcur" + e);
            return new ServiceError(404,"Exception Occur while List Creating!!!");
        }

        List<com.apu.sms.business.student.entity.Student> studentBo = null;
        if (studentEo != null && studentEo.size() > 0) {
            com.apu.sms.business.student.entity.Student student = new com.apu.sms.business.student.entity.Student();
            for (Student studentE : studentEo) {
                student.setFirstName(studentE.getFirstName());
                student.setLastName(studentE.getLastName());
                student.setLevelNo(studentE.getLevel());
                student.setRollNo(studentE.getRollNo());
                student.setId(studentE.getId());
                studentBo.add(student);
            }
        }
        return studentBo;
    }
    @Override
    public void updateStudent(com.apu.sms.business.student.entity.Student student) {

    }
    @Override
    public void deleteStudent(Integer id) {
    }*/

}
