package com.apu.sms.business.common.util;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * Created by shibli on 18/12/18.
 */
public class Utils {

    public static void print(String str){
        System.out.print(str);
    }
    public static void println(String str){
        System.out.println(str);
    }

    public static Integer ObjectToInteger(Object object){
        if(object==null){
            return 0;
        }
        else if(object instanceof Long){
            return ((Long) object).intValue();
        }
        else if(object instanceof BigDecimal){
            return ((BigDecimal) object).intValue();
        }
        else if(object instanceof Integer){
            return ((Integer) object).intValue();
        }
        return 0;
    }

    public static boolean isEmpty(String str){

        if(str==null || str.length()<1){
            return true;
        }
        return false;
    }
    public static boolean isNull(Integer val){
        if(val==null || val==0){
            return true;
        }
        return false;
    }

    public static String buildEqulQuery(String column,Integer value){
        if(column==null ||value==null || column.length()<1){
            return "";
        }
        return "and "+column +" = "+value;
    }
    public static String getNewUuid() {
        return UUID.randomUUID().toString();
    }

}
