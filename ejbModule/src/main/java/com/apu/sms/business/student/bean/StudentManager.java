package com.apu.sms.business.student.bean;

import com.apu.sms.business.student.entity.Student;
import com.apu.sms.ws.student.entity.SearchStudentCriteria;


import javax.ejb.Remote;

@Remote
public interface StudentManager {

    Object createStudent(Student student);
    Object readStudent(SearchStudentCriteria searchStudentCriteria);
    Object readStudentOrderByName(SearchStudentCriteria searchStudentCriteria);
    void updateStudent(Student student);
    void deleteStudent(Integer id);
}
