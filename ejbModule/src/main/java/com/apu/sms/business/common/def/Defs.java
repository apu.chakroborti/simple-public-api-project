package com.apu.sms.business.common.def;

/**
 * Created by shibli on 18/12/18.
 */
public class Defs {
    public static final String SEQ_NAME="student_id_seq";
    public static final String FUN_NAME="NextVal";
    public static final String CONTEXT_USER_SESSION_UUID="userSessionUUID";

    //for student table
    public  enum CLASS{
        CLASS_PLAY,
        CLASS_NURSARY,
        CLASS_ONE,
        CLASS_TWO,
        CLASS_THREE,
        CLASS_FOUR,
        CLASS_FIVE,
        CLASS_SIX,
        CLASS_SEVEN,
        CLASS_EIGHT,
        CLASS_NINE,
        CLASS_TEN,
        CLASS_SSC_CAN
    }

    public static final int CLASS_ONE=1;
    public static final int CLASS_TWO=2;
    public static final int CLASS_THREE=3;
    public static final int CLASS_FOUR=4;
    public static final int CLASS_FIVE=5;
    public static final int CLASS_SIX=6;
    public static final int CLASS_SEVEN=7;
    public static final int CLASS_EIGHT=8;
    public static final int CLASS_NINE=9;


}
