package com.apu.sms.business.student.entity;

import java.io.Serializable;

public class Student implements Serializable{
    private Integer id;
    private String firstName;
    private String lastName;
    private Integer rollNo;
    private Integer levelNo;

    public Student(Integer id, String firstName, String lastName, Integer rollNo, Integer levelNo) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.rollNo = rollNo;
        this.levelNo = levelNo;
    }
    public Student() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getRollNo() {
        return rollNo;
    }

    public void setRollNo(Integer rollNo) {
        this.rollNo = rollNo;
    }

    public Integer getLevelNo() {
        return levelNo;
    }

    public void setLevelNo(Integer levelNo) {
        this.levelNo = levelNo;
    }
}
