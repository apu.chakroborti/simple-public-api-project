package com.apu.sms.database.entity.user;



import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Cache(coordinationType = CacheCoordinationType.SEND_OBJECT_CHANGES)
@NamedQueries( { @NamedQuery(name = "User.findAll", query = "select o from User o"),
        @NamedQuery(name = "User.findById", query = "select o from User o where trim(o.id) = trim(:id)"),
        @NamedQuery(name = "User.findUserById", query = "select o from User o where Upper(trim(o.id)) = Upper(trim(:id))"),
}
)
@Table(name = "user")
public class User implements Serializable{

    @Id
    @Column(name = "ID", nullable = false)
    private Integer id;

    @Column(name="username")
    private String userName;


    @Column(name="firstname")
    private String firstName;

    @Column(name="lastname")
    private String lastName;

    @Column(name="age")
    private Integer age;

    @Column(name="password")
    private String password;

    public User(String userName, String firstName, String lastName, Integer age, String password) {
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.password = password;
    }

    public User() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

