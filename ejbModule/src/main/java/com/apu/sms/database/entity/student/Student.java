package com.apu.sms.database.entity.student;


import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Cache(coordinationType = CacheCoordinationType.SEND_OBJECT_CHANGES)
@NamedQueries( { @NamedQuery(name = "Student.findAll", query = "select o from Student o"),
        @NamedQuery(name = "Student.findById", query = "select o from Student o where trim(o.id) = trim(:id)"),
        @NamedQuery(name = "Student.findStudentById", query = "select o from Student o where Upper(trim(o.id)) = Upper(trim(:id))"),
        }
)
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(name = "student_id_seq",
                procedureName = "NextVal",
                parameters = {
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "student_id_seq", type =String.class),
                        @StoredProcedureParameter(mode = ParameterMode.OUT,name = "seq_val",type = Integer.class)
                })

})
@Table(name = "STUDENT")
public class Student implements Serializable{

    //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "student_id_seq")
    //@SequenceGenerator(name = "student_id_seq", sequenceName = "student_id_seq", allocationSize = 1)
    @Id
    @Column(name = "ID", nullable = false)
    private Integer id;

    @Column(name="FIRST_NAME")
    private String firstName;

    @Column(name="LAST_NAME")
    private String lastName;

    @Column(name="ROLL_NO")
    private Integer rollNo;

    @Column(name="LEVEL")
    private Integer level;

    public Student(String firstName, String lastName, Integer rollNo, Integer level) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.rollNo = rollNo;
        this.level = level;
    }

    public Student() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getRollNo() {
        return rollNo;
    }

    public void setRollNo(Integer rollNo) {
        this.rollNo = rollNo;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
