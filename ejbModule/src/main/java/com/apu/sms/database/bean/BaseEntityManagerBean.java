package com.apu.sms.database.bean;

import com.apu.sms.business.common.util.Utils;
import com.apu.sms.database.entity.student.Student;
import com.apu.sms.database.entity.user.User;
import com.apu.sms.ws.student.entity.SearchStudentCriteria;
import com.apu.sms.ws.user.entity.SearchUserCriteria;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.List;

@Stateless(name = "BaseEntityManagerFacade", mappedName = "SMSService-SMSService-BaseEntityManagerFacade")
@Local
public class BaseEntityManagerBean implements BaseEntityManager {

    @PersistenceContext(unitName = "SCHOOL")
    private EntityManager em;



    public void flush() {
        em.flush();
    }

    public <T> T findById(Class<T> entityClass, Object id) {
        return em.find(entityClass, id);
    }

    public <T> T getReference(Class<T> entityClass, Object id) {
        try {
            return em.getReference(entityClass, id);
        } catch (Exception e) {
            return null;
        }
    }

    public <T> T findByIdForUpdate(Class<T> entityClass, Object id) {
        T obj = em.find(entityClass, id);
        if (obj != null) {
            em.refresh(obj);
        }

        return obj;
    }



    public void refresh(Object object) {
        em.refresh(object);
    }

    private Object getSingleResult(Query query) {
        if (query == null) {
            return null;
        }

        query.setFirstResult(0);
        query.setMaxResults(1);

        List<Object> resultList = query.getResultList();
        if ((resultList == null) || (resultList.size() < 1)) {
            return null;
        } else {
            return resultList.get(0);
        }
    }

    public <T> List<T> findByQuery(String query) {
        return em.createQuery(query).getResultList();
    }


    public <T> List<T> findByQuery(String query, Integer startIndex, Integer limit) {
        if (startIndex == null || startIndex.intValue() < 0) {
            startIndex = new Integer(0);
        }

        if (limit == null) {
            limit = new Integer(100);
        }

        return em.createQuery(query)
                .setFirstResult(startIndex)
                .setMaxResults(limit)
                .getResultList();
    }

    public <T> List<T> findByQuery(String query, Integer startIndex) {
        if (startIndex == null || startIndex.intValue() < 0) {
            startIndex = new Integer(0);
        }

        return em.createQuery(query)
                .setFirstResult(startIndex)
                .getResultList();
    }

    public Integer findCountByQuery(String query) {
        Long count = (Long)em.createQuery(query).getSingleResult();
        return new Integer(count.intValue());
    }

    public Integer findCountByNativeQuery(String query) {
        Long count = ((BigDecimal)em.createNativeQuery(query).getSingleResult()).longValue();
        return new Integer(count.intValue());
    }

    public Object queryByRange(String jpqlStmt, int firstResult, int maxResults) {
        Query query = em.createQuery(jpqlStmt);
        if (firstResult > 0) {
            query = query.setFirstResult(firstResult);
        }
        if (maxResults > 0) {
            query = query.setMaxResults(maxResults);
        }
        return query.getResultList();
    }

    public Student persistStudent(Student student) {
        em.persist(student);
        em.flush();
        return student;
    }
    public User persistUser(User user) {
        em.persist(user);
        em.flush();
        return user;
    }

    public Student mergeStudent(Student student) {
        return em.merge(student);
    }

    public Integer getSequence(String sequenceName){
        StoredProcedureQuery query = this.em.createNamedStoredProcedureQuery(sequenceName);
        query.setParameter("student_id_seq",sequenceName);
        query.execute();
        //return (Integer)query.getSingleResult();

        //return (Integer)query.getOutputParameterValue("seq_val");
        return Utils.ObjectToInteger(query.getOutputParameterValue("seq_val"));
    }

   public  <T> List<T> findByQuery(SearchStudentCriteria searchStudentCriteria){
        String selectQuery="select ID , FIRST_NAME , LAST_NAME , ROLL_NO , LEVEL FROM STUDENT ";
        String whereQuery=" where 1 = 1 ";

        if(!Utils.isNull(searchStudentCriteria.getId())){
            whereQuery=whereQuery+Utils.buildEqulQuery("ID",searchStudentCriteria.getId());
        }

        Utils.println(selectQuery+whereQuery);
        Query query=em.createNativeQuery(selectQuery+whereQuery);

        if(!Utils.isNull(searchStudentCriteria.getStartIndex())){
            query.setFirstResult(searchStudentCriteria.getStartIndex());
        }
        else{
            query.setFirstResult(0);
        }

       if(!Utils.isNull(searchStudentCriteria.getMaxResult())){
           query.setMaxResults(searchStudentCriteria.getMaxResult());
       }
       else{
           query.setMaxResults(100);
       }

        return query.getResultList();
   }


    public  <T> List<T> findByCriteriaQuery(SearchStudentCriteria searchStudentCriteria){
        String selectQuery="select ID , FIRST_NAME , LAST_NAME , ROLL_NO , LEVEL FROM STUDENT ";
        String whereQuery=" where 1 = 1 ";


        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Student> q = cb.createQuery(Student.class);
        Root<Student> c = q.from(Student.class);
        q.select(c);
        q.orderBy(cb.asc(c.get("FIRST_NAME")), cb.desc(c.get("LAST_NAME")));
        return null;

        /*
        if(!Utils.isNull(searchStudentCriteria.getId())){
            whereQuery=whereQuery+Utils.buildEqulQuery("ID",searchStudentCriteria.getId());
        }

        Utils.println(selectQuery+whereQuery);
        Query query=em.createNativeQuery(selectQuery+whereQuery);

        if(!Utils.isNull(searchStudentCriteria.getStartIndex())){
            query.setFirstResult(searchStudentCriteria.getStartIndex());
        }
        else{
            query.setFirstResult(0);
        }

        if(!Utils.isNull(searchStudentCriteria.getMaxResult())){
            query.setMaxResults(searchStudentCriteria.getMaxResult());
        }
        else{
            query.setMaxResults(100);
        }

        return query.getResultList();*/
    }

    public  <T> List<T> findByUserQuery(SearchUserCriteria searchUserCriteria){
        String selectQuery="select id , username , firstname , lastname,age ,password FROM user ";
        String whereQuery=" where 1 = 1 ";

        if(!Utils.isNull(searchUserCriteria.getId())){
            whereQuery=whereQuery+Utils.buildEqulQuery("id",searchUserCriteria.getId());
        }

        Utils.println(selectQuery+whereQuery);
        Query query=em.createNativeQuery(selectQuery+whereQuery);

        if(!Utils.isNull(searchUserCriteria.getStartIndex())){
            query.setFirstResult(searchUserCriteria.getStartIndex());
        }
        else{
            query.setFirstResult(0);
        }

        if(!Utils.isNull(searchUserCriteria.getMaxResult())){
            query.setMaxResults(searchUserCriteria.getMaxResult());
        }
        else{
            query.setMaxResults(100);
        }

        return query.getResultList();
    }
}
