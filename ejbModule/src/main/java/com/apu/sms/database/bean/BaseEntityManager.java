package com.apu.sms.database.bean;

import com.apu.sms.database.entity.student.Student;
import com.apu.sms.database.entity.user.User;
import com.apu.sms.ws.student.entity.SearchStudentCriteria;
import com.apu.sms.ws.user.entity.SearchUserCriteria;

import java.util.List;

/**
 * Created by shibli on 19/12/18.
 */
public interface BaseEntityManager {
    void flush();

    void refresh(Object object);

    <T> T findById(Class<T> entityClass, Object id);

    <T> T findByIdForUpdate(Class<T> entityClass, Object id);

    Student persistStudent(Student student);
    User persistUser(User user);

    Student mergeStudent(Student student);

    <T> T getReference(Class<T> entityClass, Object id);

    <T> List<T> findByQuery(String query);


    <T> List<T> findByQuery(String query, Integer startIndex, Integer limit);

    <T> List<T> findByQuery(String query, Integer startIndex);

    Integer getSequence(String sequenceName);
    <T> List<T> findByQuery(SearchStudentCriteria searchStudentCriteria);
    <T> List<T> findByCriteriaQuery(SearchStudentCriteria searchStudentCriteria);

    <T> List<T> findByUserQuery(SearchUserCriteria searchUserCriteria);
}
